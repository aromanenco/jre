module JRE
  module JAPI
    module Java
      module Lang
        class Object
          def self.this_name
            'java/lang/Object'
          end

          def self.super_name
            nil
          end

          def self.call_j_method(method_name, signature, *args)
            if method_name == '<init>' && signature == '()V'
              handle_default_constructor(args[0])
            else
              raise "Unimplemented method: #{method_name} #{signature}"
            end
          end

          def self.handle_default_constructor(java_object)
            return  # do nothing
          end

          def self.get_j_field(name)
            raise "Field not found: #{name}"
          end
        end
      end
    end
  end
end
