module JRE
  module CPTools
    def self.unpack_CONSTANT_Methodref(cp, cp_entry)
      class_entry = cp[cp_entry[:class_index]]
      name_type_entry = cp[cp_entry[:name_and_type_index]]
      class_name = unpack_CONSTANT_Class(cp, class_entry)
      method_name, method_type = unpack_CONSTANT_NameAndType(cp, name_type_entry)
      return class_name, method_name, method_type
    end

    def self.unpack_CONSTANT_Fieldref(cp, cp_entry)
      class_entry = cp[cp_entry[:class_index]]
      name_type_entry = cp[cp_entry[:name_and_type_index]]
      class_name = unpack_CONSTANT_Class(cp, class_entry)
      field_name, field_type = unpack_CONSTANT_NameAndType(cp, name_type_entry)
      return class_name, field_name, field_type
    end

    def self.unpack_CONSTANT_Class(cp, cp_entry)
      name_entry = cp[cp_entry[:name_index]]
      name_entry[:string]
    end

    def self.unpack_CONSTANT_NameAndType(cp, cp_entry)
      name = cp[cp_entry[:name_index]][:string]
      type = cp[cp_entry[:descriptor_index]][:string]
      return name, type
    end

    def self.unpack_method_signature(signature)
      params = []
      index = 1
      while index < signature.size do
        if signature[index] == 'L'
          n = signature.index(';', index)
          params << signature[index..n]
          index = n
        elsif signature[index] == ')'
          index += 1
          break
        else
          params << signature[index]
        end
        index += 1
      end
      return_type = signature[index..signature.size - 1]
      return params, return_type
    end
  end
end
