require 'jre/class_loader/class_init'
require 'jre/code_executor'
require 'jre/java_object'
require 'jre/types/long'
require 'jre/types/type_utils'

module JRE
  class JavaClass
    include JRE::ClassLoader::ClassInit

    attr_reader :major_version, :constant_pool, :access_flags,
      :this_name, :super_name,
      :interfaces,
      :jfields,
      :jmethods
    attr_accessor :vm

    def initialize(values = {})
      @major_version = values[:major_version]
      @constant_pool = values[:constant_pool]
      @access_flags = values[:access_flags]
      init_this_name(values[:this_class])
      init_super_name(values[:super_class])
      init_interfaces(values[:interfaces])
      init_fields(values[:fields])
      init_methods(values[:methods])
    end

    def call_static_constructor
      call_j_method('<clinit>', '()V') if jmethods['<clinit>'] && jmethods['<clinit>']['()V']
    end

    def call(name, signature, *args)
      klass = method_owner(name, signature)
      method = klass.jmethods[name] && klass.jmethods[name][signature]
      raise "Not a static method" unless method[:access_flags].include?(:ACC_STATIC)
      _, return_type = JRE::CPTools.unpack_method_signature(signature)
      value = call_j_method(name, signature, *args)
      vm.java2ruby(return_type, value)
    end

    def call_j_method(name, signature, *args)
      klass = method_owner(name, signature)
      method = klass.jmethods[name] && klass.jmethods[name][signature]
      raise "Not yet, todo discovery: #{name} #{signature}" unless method
      local_vars, return_type = make_local_vars(signature, args, method[:access_flags].include?(:ACC_STATIC))
      JRE::CodeExecutor.run(klass, method, local_vars)
    end

    def get_field(name)
      type, value = get_j_field(name)
      vm.java2ruby(type, value)
    end

    def discover_field_type(name)
      return jfields[name][:type] if jfields.has_key? name
      vm.get_class(super_name).discover_field_type(name)
    end

    def method_owner(name, signature)
      return self if jmethods[name] && jmethods[name][signature]
      vm.get_class(super_name).method_owner(name, signature)
    end

    def get_j_field(name)
      return jfields[name][:type], jfields[name][:value] if jfields.include? name
      sklass = vm.get_class(super_name)
      return sklass.get_j_field(name)
    end

    def set_j_field(name, value)
      raise 'ToDo proper discovery' unless jfields.include? name
      jfields[name][:value] = value
    end

    def init_object
      object = JRE::JavaObject.new(self)
      local_vars = {0 => object}
      JRE::CodeExecutor.run(self, jmethods['<init>']['()V'], local_vars)
      object
    end

    private

    def make_local_vars(signature, args, for_static_method)
      params, return_type = JRE::CPTools.unpack_method_signature(signature)
      params.unshift('L') unless for_static_method
      local_vars = {}
      local_index = 0
      params.each_with_index do |p, i|
        local_vars[local_index] = args[i]
        local_index += 1
        local_index += 1 if ['D', 'J'].include? p
      end
      return local_vars, return_type
    end
  end
end
