require 'jre/class_loader/attrs_reader'

module JRE
  module ClassLoader
    module MethodsReader
      def self.read(bytecode, offset, constant_pool)
        methods_count = bytecode[offset..offset+1].unpack("n")[0]
        offset += 2
        methods = []
        methods_count.times do
          method = {}
          method[:access_flags] = access_flags(bytecode, offset)
          offset += 2
          method[:name_index] = bytecode[offset..offset+1].unpack("n")[0]
          offset += 2
          method[:descriptor_index] = bytecode[offset..offset+1].unpack("n")[0]
          offset += 2
          attrs, offset = AttrsReader.read_attributes(bytecode, offset, constant_pool)
          method[:attributes] = attrs
          methods << method
        end
        return methods, offset
      end

      private

      def self.access_flags(bytecode, offset)
        b = bytecode[offset..offset+1].unpack("n")[0]
        flags = []
        flags << :ACC_PUBLIC        if b & 0x0001 > 0
        flags << :ACC_PRIVATE       if b & 0x0002 > 0
        flags << :ACC_PROTECTED     if b & 0x0004 > 0
        flags << :ACC_STATIC        if b & 0x0008 > 0
        flags << :ACC_FINAL         if b & 0x0010 > 0
        flags << :ACC_SYNCHRONIZED  if b & 0x0020 > 0
        flags << :ACC_BRIDGE        if b & 0x0040 > 0
        flags << :ACC_VARARGS       if b & 0x0080 > 0
        flags << :ACC_NATIVE        if b & 0x0100 > 0
        flags << :ACC_ABSTRACT      if b & 0x0400 > 0
        flags << :ACC_STRICT        if b & 0x0800 > 0
        flags << :ACC_SYNTHETIC     if b & 0x1000 > 0
        flags
      end
    end
  end
end
