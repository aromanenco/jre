require 'jre/class_loader/mutf8'

module JRE
  module ClassLoader
    module ConstantPoolReader

      def self.const_ref(type)
        return lambda do |bytecode, offset|
          class_index = bytecode[offset..offset+1].unpack("n")[0]
          offset += 2
          name_and_type_index = bytecode[offset..offset+1].unpack("n")[0]
          offset += 2
          return {
            class_index: class_index,
            name_and_type_index: name_and_type_index,
            type: type
            }, offset, 1
        end
      end

      ITEMS = {
        1 => lambda do |bytecode, offset|  # CONSTANT_Utf8
          length = bytecode[offset..offset+1].unpack("n")[0]
          offset += 2
          encoded = bytecode[offset .. offset + length - 1]
          offset += length
          return {string: JRE::MUTF8.decode(encoded), type: :CONSTANT_Utf8}, offset, 1
        end,
        5 => lambda do |bytecode, offset|  # CONSTANT_Long
          high = bytecode[offset..offset+3].unpack("N")[0]
          low = bytecode[offset+4..offset+7].unpack("N")[0]
          offset += 8
          return {value: (high << 32) + low, type: :CONSTANT_Long}, offset, 2
        end,
        7 => lambda do |bytecode, offset|  # CONSTANT_Class
          name_index = bytecode[offset..offset+1].unpack("n")[0]
          offset += 2
          return {name_index: name_index, type: :CONSTANT_Class}, offset, 1
        end,
        8 => lambda do |bytecode, offset|  # CONSTANT_String
          string_index = bytecode[offset..offset+1].unpack("n")[0]
          offset += 2
          return {string_index: string_index, type: :CONSTANT_String}, offset, 1
        end,
        9  => const_ref(:CONSTANT_Fieldref),  # CONSTANT_Fieldref
        10 => const_ref(:CONSTANT_Methodref),  # CONSTANT_Methodref
        11 => const_ref(:CONSTANT_InterfaceMethodref_info),  # CONSTANT_InterfaceMethodref_info
        12 => lambda do |bytecode, offset|  # CONSTANT_NameAndType
          name_index = bytecode[offset..offset+1].unpack("n")[0]
          offset += 2
          descriptor_index = bytecode[offset..offset+1].unpack("n")[0]
          offset += 2
          return {
            name_index: name_index,
            descriptor_index: descriptor_index,
            type: :CONSTANT_NameAndType
            }, offset, 1
        end
      }  # cp items hash

      def self.read(bytecode)
        cp_count = bytecode[8..9].unpack("n")[0]
        offset = 10
        cp_index = 1
        cp = {}
        while cp_index < cp_count do
          tag = bytecode[offset].unpack("C")[0]
          offset += 1
          item_reader = ITEMS[tag] || Proc.new{raise "Unsupported tag: #{tag}"}
          item, offset, delta = item_reader.call(bytecode, offset)
          cp[cp_index] = item
          cp_index += delta
        end
        return cp, offset
      end

    end
  end
end
