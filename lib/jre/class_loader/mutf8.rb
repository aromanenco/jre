module JRE
  module MUTF8
    def self.decode(encoded_bytes)
      index = 0
      text = StringIO.new
      while index < encoded_bytes.length do
        c = encoded_bytes[index].unpack("C")[0]
        if (c >> 7) == 0
          text << c.chr
          index += 1
        elsif (c >> 5) == 0b110
          b = encoded_bytes[index + 1].unpack("C")[0]
          c = ((c & 0x1f) << 6) + (b & 0x3f)
          text << [c].pack("U")
          index += 2
        elsif (c >> 4) == 0b1110
          y = encoded_bytes[index + 1].unpack("C")[0]
          z = encoded_bytes[index + 2].unpack("C")[0]
          c = ((c & 0xf) << 12) + ((y & 0x3f) << 6) + (z & 0x3f)
          text << [c].pack("U")
          index += 3
        elsif c == 0b11101101
          v = encoded_bytes[index + 1].unpack("C")[0]
          w = encoded_bytes[index + 2].unpack("C")[0]
          # [index + 3] No need, this is unused byte
          y = encoded_bytes[index + 4].unpack("C")[0]
          z = encoded_bytes[index + 5].unpack("C")[0]
          c = 0x10000 + ((v & 0x0f) << 16) + ((w & 0x3f) << 10) + ((y & 0x0f) << 6) + (z & 0x3f)
          text << [c].pack("U")
          index += 6
        else
          raise "This raise will never happen..."
        end
      end
      text.string
    end
  end
end
