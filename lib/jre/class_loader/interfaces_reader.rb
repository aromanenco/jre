module JRE
  module ClassLoader
    module InterfacesReader
      def self.read(bytecode, offset)
        interfaces_count = bytecode[offset..offset+1].unpack("n")[0]
        offset += 2
        interfaces = []
        interfaces_count.times do
          cp_index = bytecode[offset..offset+1].unpack("n")[0]
          interfaces << cp_index
          offset += 2
        end
        return interfaces, offset
      end
    end
  end
end
