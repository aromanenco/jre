require 'jre/class_loader/attrs_reader'

module JRE
  module ClassLoader
    module FieldsReader
      def self.read(bytecode, offset, constant_pool)
        fields_count = bytecode[offset..offset+1].unpack("n")[0]
        offset += 2
        fields = []
        fields_count.times do
          flags = bytecode[offset..offset+1].unpack("n")[0]
          access_flags = parse(flags)
          offset += 2
          name_index = bytecode[offset..offset+1].unpack("n")[0]
          offset += 2
          descriptor_index = bytecode[offset..offset+1].unpack("n")[0]
          offset += 2
          attrs, offset = AttrsReader.read_attributes(bytecode, offset, constant_pool)
          fields << {access_flags: access_flags,
            name_index: name_index,
            descriptor_index: descriptor_index,
            attributes: attrs}
        end
        return fields, offset
      end

      private

      def self.parse(flags)
        access_flags = []
        access_flags << :ACC_PUBLIC if flags & 0x0001 > 0
        access_flags << :ACC_PRIVATE if flags & 0x0002 > 0
        access_flags << :ACC_PROTECTED if flags & 0x0004 > 0
        access_flags << :ACC_STATIC if flags & 0x0008 > 0
        access_flags << :ACC_FINAL if flags & 0x0010 > 0
        access_flags << :ACC_VOLATILE if flags & 0x0040 > 0
        access_flags << :ACC_TRANSIENT if flags & 0x0080 > 0
        access_flags << :ACC_SYNTHETIC if flags & 0x1000 > 0
        access_flags << :ACC_ENUM if flags & 0x4000 > 0
        access_flags
      end
    end
  end
end
