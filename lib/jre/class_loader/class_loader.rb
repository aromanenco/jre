require 'jre/java_class'
require 'jre/class_loader/cp_reader'
require 'jre/class_loader/interfaces_reader'
require 'jre/class_loader/fields_reader'
require 'jre/class_loader/methods_reader'

module JRE
  module ClassLoader
    CAFEBABE = 3405691582
    MAJOR_VERSION = 52

    class << self
      def load_file(file_name)
        bytecode = File.binread(file_name)
        return create_class(bytecode)
      end

      private

      def create_class(bytecode)
        raise_if_no_magic(bytecode)
        raise_if_major_ver_wrong(bytecode)
        constant_pool, offset = read_constant_pool(bytecode)
        access_flags, offset = read_access_flags(bytecode, offset)
        this_class, super_class, offset = read_this_and_super(bytecode, offset)
        interfaces, offset = read_interfaces(bytecode, offset)
        fields, offset = read_fields(bytecode, offset, constant_pool)
        methods, offset = read_methods(bytecode, offset, constant_pool)
        # ignore all attributes
        return JRE::JavaClass.new(
          major_version: MAJOR_VERSION,
          constant_pool: constant_pool,
          access_flags: access_flags,
          this_class: this_class,
          super_class: super_class,
          interfaces: interfaces,
          fields: fields,
          methods: methods)
      end

      def raise_if_no_magic(bytecode)
        raise "This is not compiled java class" if
          bytecode[0..3].unpack('N')[0] != CAFEBABE
      end

      def raise_if_major_ver_wrong(bytecode)
        raise "Only java 8 classes are supported" if
          bytecode[6..7].unpack("n") != [MAJOR_VERSION]
      end

      def read_constant_pool(bytecode)
        ConstantPoolReader.read(bytecode)
      end

      def read_access_flags(bytecode, offset)
        b = bytecode[offset..offset+1].unpack("n")[0]
        flags = []
        flags << :ACC_PUBLIC if b & 0x0001 > 0
        flags << :ACC_FINAL if b & 0x0010 > 0
        flags << :ACC_SUPER if b & 0x0020 > 0
        flags << :ACC_INTERFACE if b & 0x0200 > 0
        flags << :ACC_ABSTRACT if b & 0x0400 > 0
        flags << :ACC_SYNTHETIC if b & 0x1000 > 0
        flags << :ACC_ANNOTATION if b & 0x2000 > 0
        flags << :ACC_ENUM if b & 0x4000 > 0
        return flags, offset + 2
      end

      def read_this_and_super(bytecode, offset)
        t = bytecode[offset..offset+1].unpack("n")[0]
        s = bytecode[offset+2..offset+3].unpack("n")[0]
        return t, s, offset + 4
      end

      def read_interfaces(bytecode, offset)
        InterfacesReader.read(bytecode, offset)
      end

      def read_fields(bytecode, offset, constant_pool)
        FieldsReader.read(bytecode, offset, constant_pool)
      end

      def read_methods(bytecode, offset, constant_pool)
        MethodsReader.read(bytecode, offset, constant_pool)
      end
    end
  end
end
