module JRE
  module ClassLoader
    module AttrsReader
      def self.read_attributes(bytecode, offset, constant_pool)
        attrs_count = bytecode[offset..offset+1].unpack("n")[0]
        attrs = {}
        offset += 2
        attrs_count.times do
          attribute_name_index = bytecode[offset..offset+1].unpack("n")[0]
          offset += 2
          name = constant_pool[attribute_name_index][:string]
          case name
            when 'ConstantValue'
                attrs[:ConstantValue] = parse_constant_value(bytecode, offset)
            when 'Code'
                attrs[:Code] = parse_code(bytecode, offset, constant_pool)
          end
          len = bytecode[offset..offset+3].unpack("N")[0]
          offset += 4 + len
        end
        return attrs, offset
      end

      private

      def self.parse_constant_value(bytecode, offset)
        constantvalue_index = bytecode[offset+4..offset+5].unpack("n")[0]
        {attribute_name: :ConstantValue, constantvalue_index: constantvalue_index}
      end

      def self.parse_code(bytecode, offset, constant_pool)
        offset += 4  # no need in attribute_length
        offset += 2  # no need in max_stack
        offset += 2  # no need in max_locals
        code_length = bytecode[offset..offset+3].unpack("N")[0]
        offset += 4
        code = bytecode[offset..offset+code_length-1]
        offset += code_length
        exception_table_length = bytecode[offset..offset+1].unpack("n")[0]
        offset += 2
        exceptions = []
        exception_table_length.times do
          start_pc= bytecode[offset..offset+1].unpack("n")[0]
          offset += 2
          end_pc = bytecode[offset..offset+1].unpack("n")[0]
          offset += 2
          handler_pc = bytecode[offset..offset+1].unpack("n")[0]
          offset += 2
          catch_type = bytecode[offset..offset+1].unpack("n")[0]
          offset += 2
          exceptions << {start_pc: start_pc, end_pc: end_pc, handler_pc: handler_pc, catch_type: catch_type}
        end
        attrs, offset = read_attributes(bytecode, offset, constant_pool)
        {code: code, exceptions_table: exceptions, attributes: attrs}
      end
    end
  end
end
