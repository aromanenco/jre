module JRE
  module ClassLoader
    module ClassInit
      def init_this_name(cp_pointer)
        name_index = constant_pool[cp_pointer][:name_index]
        @this_name = constant_pool[name_index][:string]
      end

      def init_super_name(cp_pointer)
        name_index = constant_pool[cp_pointer][:name_index]
        @super_name = constant_pool[name_index][:string]
      end

      def init_interfaces(cp_pointers)
        @interfaces = []
        cp_pointers.each do |pointer|
          name_index = constant_pool[pointer][:name_index]
          @interfaces << constant_pool[name_index][:string]
        end
      end

      def init_fields(fields)
        @jfields = {}
        fields.each do |field|
          name = constant_pool[field[:name_index]][:string]
          type = constant_pool[field[:descriptor_index]][:string]
          access_flags = field[:access_flags]

          @jfields[name] = {
            type: type,
            access_flags: access_flags
          }
          if field[:access_flags].include?(:ACC_STATIC)
            @jfields[name][:value] = resolve_const_value(field) || JRE::Type::default_value(type)
          end
        end
      end

      def resolve_const_value(field)
        value = nil
        if index = field[:attributes] && field[:attributes][:ConstantValue] &&
            field[:attributes][:ConstantValue][:constantvalue_index]
          cp_entry = constant_pool[index]
          case cp_entry[:type]
          when :CONSTANT_Long
            value = JRE::Type::Long.new(cp_entry[:value])
          else
            raise "This const type(#{cp_entry[:type]}) is not supported yet"
          end
        end
        value
      end

      def init_methods(methods)
        @jmethods = {}
        methods.each do |method|
          normalized_method = {}
          normalized_method[:access_flags] = method[:access_flags]
          raise "ACC_NATIVE method not supported" if access_flags.include? :ACC_NATIVE
          name = constant_pool[method[:name_index]][:string]
          normalized_method[:name] = name
          type = constant_pool[method[:descriptor_index]][:string]
          normalized_method[:type] = type
          if not access_flags.include? :ACC_ABSTRACT
            code_attr = method[:attributes][:Code]
            normalized_method[:code] = code_attr[:code]
            normalized_method[:exceptions_table] = code_attr[:exceptions_table]
          end
          @jmethods[name] = {} unless @jmethods.has_key? name
          @jmethods[name][type] = normalized_method
        end
      end
    end
  end
end
