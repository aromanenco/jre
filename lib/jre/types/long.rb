module JRE
  module Type
    class Long
      attr_reader :value
      def initialize(n)
        @value = n
      end

      def to_s
        "L#{@value}"
      end
    end
  end
end
