require 'jre/types/long'

module JRE
  module Type
    def self.init_java2ruby(map)
      map['Ljava/lang/String;'] = lambda do |value|
        value
      end
      map['I'] = lambda do |value|
        value
      end
      map['J'] = lambda do |value|
        value.value
      end
      map['V'] = lambda do |value|
        :void
      end
      map
    end

    def self.default_value(type_name)
      case type_name
      when 'I'  # int
        return 0
      when 'J'  # long
        return JRE::Type::Long.new(0)
      end
      if (type_name[0] == 'L')  # object
        return nil
      end
      raise "Type not supported: #{type_name}"
    end
  end
end
