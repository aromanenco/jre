require 'jre/cp_tools'
require 'jre/types/long'

module JRE
  module CodeExecutor
    def self.run(java_class, method, local_vars = {}, stack = [])
      pc = 0
      code = method[:code]
      while pc < code.size do
        op = code[pc].unpack("C")[0]
        pc += 1
        case op
        when 0x00  # nop
          # no operation
        when 0x01  # aconst_null
          stack.push(nil)
        when 0x02  # iconst_m1
          stack.push(-1)
        when 0x03  # iconst_0
          stack.push(0)
        when 0x04  # iconst_1
          stack.push(1)
        when 0x05  # iconst_2
          stack.push(2)
        when 0x06  # iconst_3
          stack.push(3)
        when 0x07  # iconst_4
          stack.push(4)
        when 0x08  # iconst_5
          stack.push(5)
        when 0x09  # lconst_0
          stack.push(JRE::Type::Long.new(0))
        when 0x0a  # lconst_1
          stack.push(JRE::Type::Long.new(1))
        when 0x0b  # fconst_0
          raise "Op 0x0b not yet supported"
        when 0x0c  # fconst_1
          raise "Op 0x0c not yet supported"
        when 0x0d  # fconst_2
          raise "Op 0x0d not yet supported"
        when 0x0e  # dconst_0
          raise "Op 0x0e not yet supported"
        when 0x0f  # dconst_1
          raise "Op 0x0f not yet supported"
        when 0x10  # bipush
          value = code[pc].unpack("C")[0]
          pc += 1
          stack.push(value)
        when 0x11  # sipush
          raise "Op 0x11 not yet supported"
        when 0x12  # ldc
          cp_index = code[pc].unpack("C")[0]
          cp_entry = java_class.constant_pool[cp_index]
          pc += 1
          if cp_entry[:type] == :CONSTANT_String
            sutf8 = java_class.constant_pool[cp_entry[:string_index]][:string]
            ref = java_class.vm.get_string_ref(sutf8)
            stack.push(ref)
          else
            raise "This CP entry not yet supported: #{cp_entry}"
          end
        when 0x13  # ldc_w
          raise "Op 0x13 not yet supported"
        when 0x14  # ldc2_w
          raise "Op 0x14 not yet supported"
        when 0x15  # iload
          index = code[pc].unpack("C")[0]
          pc += 1
          stack.push(local_vars[index])
        when 0x16  # lload
          index = code[pc].unpack("C")[0]
          pc += 1
          stack.push(local_vars[index])
        when 0x17  # fload
          index = code[pc].unpack("C")[0]
          pc += 1
          stack.push(local_vars[index])
        when 0x18  # dload
          index = code[pc].unpack("C")[0]
          pc += 1
          stack.push(local_vars[index])
        when 0x19  # aload
          index = code[pc].unpack("C")[0]
          pc += 1
          stack.push(local_vars[index])
        when 0x1a  # iload_0
          stack.push(local_vars[0])
        when 0x1b  # iload_1
          stack.push(local_vars[1])
        when 0x1c  # iload_2
          stack.push(local_vars[2])
        when 0x1d  # iload_3
          stack.push(local_vars[3])
        when 0x1e  # lload_0
          stack.push(local_vars[0])
        when 0x1f  # lload_1
          stack.push(local_vars[1])
        when 0x20  # lload_2
          stack.push(local_vars[2])
        when 0x21  # lload_3
          stack.push(local_vars[3])
        when 0x22  # fload_0
          stack.push(local_vars[0])
        when 0x23  # fload_1
          stack.push(local_vars[1])
        when 0x24  # fload_2
          stack.push(local_vars[2])
        when 0x25  # fload_3
          stack.push(local_vars[3])
        when 0x26  # dload_0
          stack.push(local_vars[0])
        when 0x27  # dload_1
          stack.push(local_vars[1])
        when 0x28  # dload_2
          stack.push(local_vars[2])
        when 0x29  # dload_3
          stack.push(local_vars[3])
        when 0x2a  # aload_0
          stack.push(local_vars[0])
        when 0x2b  # aload_1
          stack.push(local_vars[1])
        when 0x2c  # aload_2
          stack.push(local_vars[2])
        when 0x2d  # aload_3
          stack.push(local_vars[3])
        when 0x2e  # iaload
          index = stack.pop
          array = stack.pop
          raise "Throw proper exception" if array.size <= index
          stack.push array[index]
        when 0x2f  # laload
          raise "Op 0x2f not yet supported"
        when 0x30  # faload
          raise "Op 0x30 not yet supported"
        when 0x31  # daload
          raise "Op 0x31 not yet supported"
        when 0x32  # aaload
          raise "Op 0x32 not yet supported"
        when 0x33  # baload
          raise "Op 0x33 not yet supported"
        when 0x34  # caload
          raise "Op 0x34 not yet supported"
        when 0x35  # saload
          raise "Op 0x35 not yet supported"
        when 0x36  # istore
          index = code[pc].unpack("C")[0]
          pc += 1
          local_vars[index] = stack.pop()
        when 0x37  # lstore
          index = code[pc].unpack("C")[0]
          pc += 1
          local_vars[index] = stack.pop()
        when 0x38  # fstore
          index = code[pc].unpack("C")[0]
          pc += 1
          local_vars[index] = stack.pop()
        when 0x39  # dstore
          index = code[pc].unpack("C")[0]
          pc += 1
          local_vars[index] = stack.pop()
        when 0x3a  # astore
          index = code[pc].unpack("C")[0]
          pc += 1
          local_vars[index] = stack.pop()
        when 0x3b  # istore_0
          local_vars[0] = stack.pop()
        when 0x3c  # istore_1
          local_vars[1] = stack.pop()
        when 0x3d  # istore_2
          local_vars[2] = stack.pop()
        when 0x3e  # istore_3
          local_vars[3] = stack.pop()
        when 0x3f  # lstore_0
          local_vars[0] = stack.pop()
        when 0x40  # lstore_1
          local_vars[1] = stack.pop()
        when 0x41  # lstore_2
          local_vars[2] = stack.pop()
        when 0x42  # lstore_3
          local_vars[3] = stack.pop()
        when 0x43  # fstore_0
          local_vars[0] = stack.pop()
        when 0x44  # fstore_1
          local_vars[1] = stack.pop()
        when 0x45  # fstore_2
          local_vars[2] = stack.pop()
        when 0x46  # fstore_3
          local_vars[3] = stack.pop()
        when 0x47  # dstore_0
          local_vars[0] = stack.pop()
        when 0x48  # dstore_1
          local_vars[1] = stack.pop()
        when 0x49  # dstore_2
          local_vars[2] = stack.pop()
        when 0x4a  # dstore_3
          local_vars[3] = stack.pop()
        when 0x4b  # astore_0
          local_vars[0] = stack.pop()
        when 0x4c  # astore_1
          local_vars[1] = stack.pop()
        when 0x4d  # astore_2
          local_vars[2] = stack.pop()
        when 0x4e  # astore_3
          local_vars[3] = stack.pop()
        when 0x4f  # iastore
          value = stack.pop
          index = stack.pop
          array = stack.pop
          raise "Throw proper exception" if array.size <= index
          array[index] = value
        when 0x50  # lastore
          raise "Op 0x50 not yet supported"
        when 0x51  # fastore
          raise "Op 0x51 not yet supported"
        when 0x52  # dastore
          raise "Op 0x52 not yet supported"
        when 0x53  # aastore
          raise "Op 0x53 not yet supported"
        when 0x54  # bastore
          raise "Op 0x54 not yet supported"
        when 0x55  # castore
          raise "Op 0x55 not yet supported"
        when 0x56  # sastore
          raise "Op 0x56 not yet supported"
        when 0x57  # pop
          stack.pop
        when 0x58  # pop2
          raise "Op 0x58 not yet supported"
        when 0x59  # dup
          value = stack.pop
          stack.push(value)
          stack.push(value)
        when 0x5a  # dup_x1
          raise "Op 0x5a not yet supported"
        when 0x5b  # dup_x2
          raise "Op 0x5b not yet supported"
        when 0x5c  # dup2
          raise "Op 0x5c not yet supported"
        when 0x5d  # dup2_x1
          raise "Op 0x5d not yet supported"
        when 0x5e  # dup2_x2
          raise "Op 0x5e not yet supported"
        when 0x5f  # swap
          raise "Op 0x5f not yet supported"
        when 0x60  # iadd
          v2 = stack.pop
          v1 = stack.pop
          stack.push(v1 + v2)  # TODO int range check
        when 0x61  # ladd
          raise "Op 0x61 not yet supported"
        when 0x62  # fadd
          raise "Op 0x62 not yet supported"
        when 0x63  # dadd
          raise "Op 0x63 not yet supported"
        when 0x64  # isub
          v2 = stack.pop
          v1 = stack.pop
          stack.push(v1 - v2)  # TODO int range check
        when 0x65  # lsub
          raise "Op 0x65 not yet supported"
        when 0x66  # fsub
          raise "Op 0x66 not yet supported"
        when 0x67  # dsub
          raise "Op 0x67 not yet supported"
        when 0x68  # imul
          v2 = stack.pop
          v1 = stack.pop
          stack.push(v1 * v2)  # TODO int range check
        when 0x69  # lmul
          raise "Op 0x69 not yet supported"
        when 0x6a  # fmul
          raise "Op 0x6a not yet supported"
        when 0x6b  # dmul
          raise "Op 0x6b not yet supported"
        when 0x6c  # idiv
          raise "Op 0x6c not yet supported"
        when 0x6d  # ldiv
          raise "Op 0x6d not yet supported"
        when 0x6e  # fdiv
          raise "Op 0x6e not yet supported"
        when 0x6f  # ddiv
          raise "Op 0x6f not yet supported"
        when 0x70  # irem
          raise "Op 0x70 not yet supported"
        when 0x71  # lrem
          raise "Op 0x71 not yet supported"
        when 0x72  # frem
          raise "Op 0x72 not yet supported"
        when 0x73  # drem
          raise "Op 0x73 not yet supported"
        when 0x74  # ineg
          raise "Op 0x74 not yet supported"
        when 0x75  # lneg
          raise "Op 0x75 not yet supported"
        when 0x76  # fneg
          raise "Op 0x76 not yet supported"
        when 0x77  # dneg
          raise "Op 0x77 not yet supported"
        when 0x78  # ishl
          raise "Op 0x78 not yet supported"
        when 0x79  # lshl
          raise "Op 0x79 not yet supported"
        when 0x7a  # ishr
          raise "Op 0x7a not yet supported"
        when 0x7b  # lshr
          raise "Op 0x7b not yet supported"
        when 0x7c  # iushr
          raise "Op 0x7c not yet supported"
        when 0x7d  # lushr
          raise "Op 0x7d not yet supported"
        when 0x7e  # iand
          raise "Op 0x7e not yet supported"
        when 0x7f  # land
          raise "Op 0x7f not yet supported"
        when 0x80  # ior
          raise "Op 0x80 not yet supported"
        when 0x81  # lor
          raise "Op 0x81 not yet supported"
        when 0x82  # ixor
          raise "Op 0x82 not yet supported"
        when 0x83  # lxor
          raise "Op 0x83 not yet supported"
        when 0x84  # iinc
          index = code[pc].unpack("C")[0]
          pc += 1
          const_value = code[pc].unpack("c")[0]
          pc += 1
          local_vars[index] += const_value
        when 0x85  # i2l
          value = stack.pop
          stack.push(JRE::Type::Long.new(value))
        when 0x86  # i2f
          raise "Op 0x86 not yet supported"
        when 0x87  # i2d
          raise "Op 0x87 not yet supported"
        when 0x88  # l2i
          value = stack.pop
          stack.push(value.value)
        when 0x89  # l2f
          raise "Op 0x89 not yet supported"
        when 0x8a  # l2d
          raise "Op 0x8a not yet supported"
        when 0x8b  # f2i
          raise "Op 0x8b not yet supported"
        when 0x8c  # f2l
          raise "Op 0x8c not yet supported"
        when 0x8d  # f2d
          raise "Op 0x8d not yet supported"
        when 0x8e  # d2i
          raise "Op 0x8e not yet supported"
        when 0x8f  # d2l
          raise "Op 0x8f not yet supported"
        when 0x90  # d2f
          raise "Op 0x90 not yet supported"
        when 0x91  # i2b
          raise "Op 0x91 not yet supported"
        when 0x92  # i2c
          raise "Op 0x92 not yet supported"
        when 0x93  # i2s
          raise "Op 0x93 not yet supported"
        when 0x94  # lcmp
          value2 = stack.pop
          value1 = stack.pop
          if value1.value == value2.value
            stack.push(0)
          elsif value1.value > value2.value
            stack.push(1)
          else
            stack.push(-1)
          end
        when 0x95  # fcmpl
          raise "Op 0x95 not yet supported"
        when 0x96  # fcmpg
          raise "Op 0x96 not yet supported"
        when 0x97  # dcmpl
          raise "Op 0x97 not yet supported"
        when 0x98  # dcmpg
          raise "Op 0x98 not yet supported"
        when 0x99  # ifeq
          value = stack.pop
          offset = code[pc..pc+1].unpack("s>")[0]
          pc += 2
          if value == 0
            pc += offset - 3
          end
        when 0x9a  # ifne
          raise "Op 0x9a not yet supported"
        when 0x9b  # iflt
          raise "Op 0x9b not yet supported"
        when 0x9c  # ifge
          raise "Op 0x9c not yet supported"
        when 0x9d  # ifgt
          raise "Op 0x9d not yet supported"
        when 0x9e  # ifle
          raise "Op 0x9e not yet supported"
        when 0x9f  # if_icmpeq
          raise "Op 0x9f not yet supported"
        when 0xa0  # if_icmpne
          raise "Op 0xa0 not yet supported"
        when 0xa1  # if_icmplt
          raise "Op 0xa1 not yet supported"
        when 0xa2  # if_icmpge
          offset = code[pc..pc+1].unpack("s>")[0]
          pc += 2
          value2 = stack.pop
          value1 = stack.pop
          if value1 >= value2
            pc += offset - 3
          end
        when 0xa3  # if_icmpgt
          raise "Op 0xa3 not yet supported"
        when 0xa4  # if_icmple
          offset = code[pc..pc+1].unpack("s>")[0]
          pc += 2
          value2 = stack.pop
          value1 = stack.pop
          if value1 <= value2
            pc += offset - 3
          end
        when 0xa5  # if_acmpeq
          raise "Op 0xa5 not yet supported"
        when 0xa6  # if_acmpne
          raise "Op 0xa6 not yet supported"
        when 0xa7  # goto
          offset = code[pc..pc+1].unpack("s>")[0]
          pc += offset - 1
        when 0xa8  # jsr
          raise "Op 0xa8 not yet supported"
        when 0xa9  # ret
          raise "Op 0xa9 not yet supported"
        when 0xaa  # tableswitch
          raise "Op 0xaa not yet supported"
        when 0xab  # lookupswitch
          raise "Op 0xab not yet supported"
        when 0xac  # ireturn
          value = stack.pop
          return value
        when 0xad  # lreturn
          value = stack.pop
          return value
        when 0xae  # freturn
          raise "Op 0xae not yet supported"
        when 0xaf  # dreturn
          raise "Op 0xaf not yet supported"
        when 0xb0  # areturn
          value = stack.pop
          return value
        when 0xb1  # return
          return :void
        when 0xb2  # getstatic
          raise "Op 0xb2 not yet supported"
        when 0xb3  # putstatic
          value = stack.pop
          cp_index = code[pc..pc+1].unpack("n")[0]
          pc += 2
          cp_entry = java_class.constant_pool[cp_index]
          class_name, field_name, field_type = JRE::CPTools.unpack_CONSTANT_Fieldref(java_class.constant_pool, cp_entry)
          jclass = java_class.vm.get_class(class_name)
          jclass.set_j_field(field_name, value)
        when 0xb4  # getfield
          raise "Op 0xb4 not yet supported"
        when 0xb5  # putfield
          value = stack.pop
          ref = stack.pop
          cp_index = code[pc..pc+1].unpack("n")[0]
          pc += 2
          cp_entry = java_class.constant_pool[cp_index]
          class_name, field_name, field_type = JRE::CPTools.unpack_CONSTANT_Fieldref(java_class.constant_pool, cp_entry)
          ref.set_j_field(field_name, value)
        when 0xb6  # invokevirtual
          cp_index = code[pc..pc+1].unpack("n")[0]
          pc += 2
          cp_entry = java_class.constant_pool[cp_index]
          class_name, method_name, method_type = JRE::CPTools.unpack_CONSTANT_Methodref(java_class.constant_pool, cp_entry)
          params, _ = JRE::CPTools.unpack_method_signature(method_type)
          klass = java_class.vm.get_class(class_name)
          args = []
          params.size.times {args.unshift stack.pop}
          args.unshift stack.pop
          result = klass.call_j_method(method_name, method_type, *args)
          stack.push result unless result == :void
        when 0xb7  # invokespecial
          cp_index = code[pc..pc+1].unpack("n")[0]
          pc += 2
          cp_entry = java_class.constant_pool[cp_index]
          class_name, method_name, method_type = JRE::CPTools.unpack_CONSTANT_Methodref(java_class.constant_pool, cp_entry)
          raise 'TODO' unless method_name == '<init>' && method_type == '()V'
          ref = stack.pop
          klass = java_class.vm.get_class(class_name)
          klass.call_j_method(method_name, method_type, ref)
        when 0xb8  # invokestatic
          cp_index = code[pc..pc+1].unpack("n")[0]
          pc += 2
          cp_entry = java_class.constant_pool[cp_index]
          class_name, method_name, method_type = JRE::CPTools.unpack_CONSTANT_Methodref(java_class.constant_pool, cp_entry)
          params, _ = JRE::CPTools.unpack_method_signature(method_type)
          klass = java_class.vm.get_class(class_name)
          args = []
          params.size.times {args.unshift stack.pop}
          result = klass.call_j_method(method_name, method_type, *args)
          stack.push result unless result == :void
        when 0xb9  # invokeinterface
          raise "Op 0xb9 not yet supported"
        when 0xba  # invokedynamic
          raise "Op 0xba not yet supported"
        when 0xbb  # new
          raise "Op 0xbb not yet supported"
        when 0xbc  # newarray
          atype = code[pc].unpack("C")[0]
          pc += 1
          count = stack.pop
          case atype
          when 10  # int
            stack.push Array.new(count, 0)
          else
            raise "Array type not yest supported: #{atype}"
          end
        when 0xbd  # anewarray
          raise "Op 0xbd not yet supported"
        when 0xbe  # arraylength
          array = stack.pop
          stack.push array.size
        when 0xbf  # athrow
          raise "Op 0xbf not yet supported"
        when 0xc0  # checkcast
          raise "Op 0xc0 not yet supported"
        when 0xc1  # instanceof
          raise "Op 0xc1 not yet supported"
        when 0xc2  # monitorenter
          # do nothing
        when 0xc3  # monitorexit
          # do nothing
        when 0xc4  # wide
          raise "Op 0xc4 not yet supported"
        when 0xc5  # multianewarray
          raise "Op 0xc5 not yet supported"
        when 0xc6  # ifnull
          raise "Op 0xc6 not yet supported"
        when 0xc7  # ifnonnull
          raise "Op 0xc7 not yet supported"
        when 0xc8  # goto_w
          raise "Op 0xc8 not yet supported"
        when 0xc9  # jsr_w
          raise "Op 0xc9 not yet supported"
        else
          raise "Byte code operation(0x#{op.to_s(16)}) is not supported"
        end
      end
    end
  end
end
