require 'jre/classpath'
require 'jre/class_loader/class_loader'
require 'jre/types/type_utils'

require 'jre/java_api/java/lang/object'

module JRE
  class VM
    include

    def initialize
      @classpath = JRE::ClassPath.new
      @loaded_classes = {}  # full class name(with /) -> java class
      @loaded_classes['java/lang/Object'] = JRE::JAPI::Java::Lang::Object
      @cached_strings = {}  # string -> string ref on heap
      @map_j2r = {}
      JRE::Type::init_java2ruby(@map_j2r)
    end

    def get_class(name)
      name = name.gsub(/\./,'/')
      return @loaded_classes[name] if @loaded_classes.has_key? name
      file_name = @classpath.path_for_class(name)
      jclass = JRE::ClassLoader.load_file(file_name)
      get_class(jclass.super_name)
      jclass.vm = self
      @loaded_classes[jclass.this_name] = jclass
      jclass.call_static_constructor
      jclass
    end

    def get_string_ref(string_value)
      return @cached_strings[string_value] if @cached_strings.has_key? string_value
      @cached_strings[string_value] = string_value
      @cached_strings[string_value]
    end

    def java2ruby(type, value)
      raise "No j2r mapping for #{type}" unless @map_j2r.has_key?(type)
      @map_j2r[type].call(value)
    end
  end
end
