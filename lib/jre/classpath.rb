module JRE
  class ClassPath
    def initialize
      @classes = {}  # class name to file name
    end

    def path_for_class(class_name)
      file_name = "test/#{class_name}.class"
      raise 'ToDo real implementation' unless File.exists? file_name
      file_name
    end
  end
end
