require 'jre/types/type_utils'

module JRE
  class JavaObject
    attr_reader :java_class
    def initialize(java_class)
      @java_class = java_class
      @fields = {}
      init_own_fields
    end

    def get_field(name)
      if @fields.has_key? name
        type = @java_class.discover_field_type(name)
        return @java_class.vm.java2ruby(type, @fields[name])
      else
        @java_class.get_field(name)
      end
    end

    def set_j_field(name, value)
      @fields[name] = value
    end

    def call(method_name, signature, *args)
      method_owner = @java_class.method_owner(method_name, signature)
      method = method_owner.jmethods[method_name] && method_owner.jmethods[method_name][signature]
      args.unshift(self) unless method[:access_flags].include?(:ACC_STATIC)
      java_class.call_j_method(method_name, signature, *args)
    end

    private

    def init_own_fields
      klass = @java_class
      until klass.this_name ==  'java/lang/Object' do
        klass.jfields.each do |name, field|
          @fields[name] = JRE::Type::default_value(field[:type]) unless field[:access_flags].include?(:ACC_STATIC)
        end
        klass = klass.vm.get_class(klass.super_name)
      end
    end
  end
end
