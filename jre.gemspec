Gem::Specification.new do |s|
  s.name        = 'jre'
  s.version     = '0.0.1'
  s.date        = '2015-01-09'
  s.summary     = "TBD"
  s.description = "TBD"
  s.authors     = ["Andrew Romanenco"]
  s.email       = 'andrew@romanenco.com'
  s.files       = ["lib/jre.rb",
    "lib/jre/class_loader/class_loader.rb",
    "lib/jre/class_loader/bynary_jclass.rb",
    "lib/jre/class_loader/cp_reader.rb",
    "lib/jre/class_loader/mutf8.rb",
    "lib/jre/class_loader/interfaces_reader.rb",
    "lib/jre/class_loader/fields_reader.rb",
    "lib/jre/class_loader/methods_reader.rb"]
  s.homepage    =
    'TBD'
  s.license     = 'Apache License Version 2.0'
end
