require 'minitest/autorun'
require 'jre/classpath'

class ClassPathTest < Minitest::Test
  def test_directory
    testee = JRE::ClassPath.new
    assert_equal 'test/Test.class', testee.path_for_class('Test')
  end
end
