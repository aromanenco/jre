public class Sort {
  public static void sort(int[] array) {
    boolean flag = true;
    while (flag) {
      flag = false;
      for (int i = 0; i < array.length - 1; i++) {
        if (array[i] > array[i + 1]) {
          flag = true;
          final int temp = array[i];
          array[i] = array[i + 1];
          array[i + 1] = temp;
        }
      }
    }
  }

  public static void main(String[] args) {
    final int count = Integer.parseInt(args[0]);
    final int[] array = new int[count];
    for (int i = 0; i < count; i++) {
      array[i] = count - i;
    }
    final long start = System.currentTimeMillis();
    sort(array);
    final long diff = System.currentTimeMillis() - start;
    System.out.println(diff);
  }
}
