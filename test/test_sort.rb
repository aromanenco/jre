require 'minitest/autorun'
require 'jre/vm'

class VMTest < Minitest::Test
  def test_call_static_method
    vm = JRE::VM.new
    jclass = vm.get_class('Sort')
    assert jclass
    list = [3,2,1]
    jclass.call('sort', '([I)V', list)
    assert_equal [1,2,3], list
  end
end
