require 'minitest/autorun'
require 'jre/vm'

class VMTest < Minitest::Test
  def test_static_constructor
    vm = JRE::VM.new
    jclass = vm.get_class('Test')
    assert jclass
    assert_equal 'Ljava/lang/String;', jclass.jfields['SCONST'][:type]
    assert_equal 'J', jclass.jfields['serialVersionUID'][:type]
    assert_equal 'I', jclass.jfields['ICONST'][:type]
    assert_equal 'It works!', jclass.jfields['SCONST'][:value]
    assert_equal 42, jclass.jfields['serialVersionUID'][:value].value
    assert_equal 99, jclass.jfields['ICONST'][:value]
  end

  def test_get_static_field
    vm = JRE::VM.new
    jclass = vm.get_class('Test')
    assert jclass
    assert_equal 'It works!', jclass.get_field('SCONST')
    assert_equal 42, jclass.get_field('serialVersionUID')
    assert_equal 99, jclass.get_field('ICONST')
  end

  def test_call_static_method
    vm = JRE::VM.new
    jclass = vm.get_class('Test')
    assert jclass
    result = jclass.call('add', '(II)I', 1, 2)
    assert_equal 3, result
    result = jclass.call('sumWithArray', '(II)I', 10, 11)
    assert_equal 21, result
  end

  def test_call_instance_method
    vm = JRE::VM.new
    jclass = vm.get_class('Test')
    assert jclass
    o = jclass.init_object
    assert o
    assert_equal 42, o.get_field('ivar')
    assert_equal jclass, o.java_class()
    result = o.call('doubleUp', '(I)I', 42)
    assert_equal 84, result
  end

  def test_get_static_field_via_object
    vm = JRE::VM.new
    jclass = vm.get_class('Test')
    assert jclass
    o = jclass.init_object
    assert o
    assert_equal 99, o.get_field('ICONST')
  end

  def test_inherited_fields_and_methods
    vm = JRE::VM.new
    jclass = vm.get_class('ClassB')
    assert jclass
    assert_equal 1, jclass.get_field('svar')
    assert_equal 5, jclass.call('doubleInc', '(I)I', 3)
    assert_equal 100, jclass.call('inc', '(I)I', 99)
    o = jclass.init_object
    assert o
    assert_equal 2, o.get_field('ivar')
    assert_equal 98, o.call('dec', '(I)I', 99)
    assert_equal 19, o.call('doubleDec', '(I)I', 21)
    assert_equal 88, o.call('doubleInc', '(I)I', 86)
  end
end
