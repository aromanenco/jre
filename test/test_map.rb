require 'minitest/autorun'
require 'jre/types/type_utils'
require 'jre/types/long'

class TypeMapTest < Minitest::Test
  def setup
    @map = {}
    JRE::Type::init_java2ruby(@map)
  end

  def test_j2r_int
    value = 42
    assert_equal 42, @map['I'].call(value)
  end

  def test_j2r_long
    value = JRE::Type::Long.new(99)
    assert_equal 99, @map['J'].call(value)
  end

  def test_j2r_string
    value = "some string"
    assert_equal "some string", @map['Ljava/lang/String;'].call(value)
  end
end
