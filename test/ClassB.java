public class ClassB extends ClassA {

	public static void main(String args[]) {
		ClassB b = new ClassB();
		System.out.println(ClassB.svar);
		System.out.println(b.ivar);
		System.out.println(ClassB.inc(9));
		System.out.println(b.dec(17));
	}

	static int doubleInc(int i) {
		return inc(inc(i));
	}

	int doubleDec(int i) {
		return dec(dec(i));
	}

}
