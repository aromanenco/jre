public class Test implements java.io.Serializable {

	protected static final long serialVersionUID = 42L;

	public static String SCONST = "It works!";
	public static int ICONST = 99;
	public int ivar = 42;

	public static void main(String[] args) {
		System.out.println("It works!");
	}

	public int getConst() {
		return ICONST;
	}

	public static int add(int a, int b) {
		return a + b;
	}

	public int doubleUp(int a) {
		return a*2;
	}

	public static int sumWithArray(int a, int b) {
		final int[] array = new int[2];
		array[0] = a;
		array[1] = b;
		int sum = 0;
		for (int i = 0; i < array.length; i++) {
			sum += array[i];
		}
		return sum;
	}

}
