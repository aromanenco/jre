require 'minitest/autorun'
require 'jre/code_executor'

class ClassLoaderTest < Minitest::Test
  def test_nop_0x00
    stack = []
    JRE::CodeExecutor.run(nil, {code: [[0x00].pack('C')]}, nil, stack)
    assert_equal [], stack
  end

  def test_aconst_null_0x01
    stack = []
    JRE::CodeExecutor.run(nil, {code: [[0x01].pack('C')]}, nil, stack)
    assert_equal [nil], stack
  end

  def test_iconst_m1_0x02
    stack = []
    JRE::CodeExecutor.run(nil, {code: [[0x02].pack('C')]}, nil, stack)
    assert_equal [-1], stack
  end

  def test_iconst_0_0x03
    stack = []
    JRE::CodeExecutor.run(nil, {code: [[0x03].pack('C')]}, nil, stack)
    assert_equal [0], stack
  end

  def test_iconst_1_0x04
    stack = []
    JRE::CodeExecutor.run(nil, {code: [[0x04].pack('C')]}, nil, stack)
    assert_equal [1], stack
  end

  def test_iconst_2_0x05
    stack = []
    JRE::CodeExecutor.run(nil, {code: [[0x05].pack('C')]}, nil, stack)
    assert_equal [2], stack
  end

  def test_iconst_3_0x06
    stack = []
    JRE::CodeExecutor.run(nil, {code: [[0x06].pack('C')]}, nil, stack)
    assert_equal [3], stack
  end

  def test_iconst_4_0x07
    stack = []
    JRE::CodeExecutor.run(nil, {code: [[0x07].pack('C')]}, nil, stack)
    assert_equal [4], stack
  end

  def test_iconst_5_0x08
    stack = []
    JRE::CodeExecutor.run(nil, {code: [[0x08].pack('C')]}, nil, stack)
    assert_equal [5], stack
  end

  def test_lconst_0_0x09
    stack = []
    JRE::CodeExecutor.run(nil, {code: [[0x09].pack('C')]}, nil, stack)
    assert_equal 1, stack.size
    assert_equal 0, stack[0].value
  end

  def test_lconst_1_0x0a
    stack = []
    JRE::CodeExecutor.run(nil, {code: [[0x0a].pack('C')]}, nil, stack)
    assert_equal 1, stack.size
    assert_equal 1, stack[0].value
  end

  def test_bipush_0x10
    stack = []
    JRE::CodeExecutor.run(nil, {code: [[0x10].pack('C'), [5].pack('C')]}, nil, stack)
    assert_equal [5], stack
  end

  def test_iload_0x15
    stack = []
    JRE::CodeExecutor.run(nil, {code: [[0x15].pack('C'), [2].pack('C')]}, ({2 => 99}), stack)
    assert_equal [99], stack
  end

  def test_lload_0x16
    stack = []
    JRE::CodeExecutor.run(nil, {code: [[0x16].pack('C'), [2].pack('C')]}, ({2 => 99}), stack)
    assert_equal [99], stack
  end

  def test_fload_0x17
    stack = []
    JRE::CodeExecutor.run(nil, {code: [[0x17].pack('C'), [2].pack('C')]}, ({2 => 99}), stack)
    assert_equal [99], stack
  end

  def test_dload_0x18
    stack = []
    JRE::CodeExecutor.run(nil, {code: [[0x18].pack('C'), [2].pack('C')]}, ({2 => 99}), stack)
    assert_equal [99], stack
  end

  def test_aload_0x19
    stack = []
    JRE::CodeExecutor.run(nil, {code: [[0x19].pack('C'), [2].pack('C')]}, ({2 => 99}), stack)
    assert_equal [99], stack
  end

  def test_iload_0_0x1a
    stack = []
    JRE::CodeExecutor.run(nil, {code: [[0x1a].pack('C')]}, ({0 => 99}), stack)
    assert_equal [99], stack
  end

  def test_iload_1_0x1b
    stack = []
    JRE::CodeExecutor.run(nil, {code: [[0x1b].pack('C')]}, ({1 => 99}), stack)
    assert_equal [99], stack
  end

  def test_iload_2_0x1c
    stack = []
    JRE::CodeExecutor.run(nil, {code: [[0x1c].pack('C')]}, ({2 => 99}), stack)
    assert_equal [99], stack
  end

  def test_iload_3_0x1d
    stack = []
    JRE::CodeExecutor.run(nil, {code: [[0x1d].pack('C')]}, ({3 => 99}), stack)
    assert_equal [99], stack
  end

  def test_lload_0_0x1e
    stack = []
    JRE::CodeExecutor.run(nil, {code: [[0x1e].pack('C')]}, ({0 => 99}), stack)
    assert_equal [99], stack
  end

  def test_lload_1_0x1f
    stack = []
    JRE::CodeExecutor.run(nil, {code: [[0x1f].pack('C')]}, ({1 => 99}), stack)
    assert_equal [99], stack
  end

  def test_lload_2_0x20
    stack = []
    JRE::CodeExecutor.run(nil, {code: [[0x20].pack('C')]}, ({2 => 99}), stack)
    assert_equal [99], stack
  end

  def test_lload_3_0x21
    stack = []
    JRE::CodeExecutor.run(nil, {code: [[0x21].pack('C')]}, ({3 => 99}), stack)
    assert_equal [99], stack
  end

  def test_fload_0_0x22
    stack = []
    JRE::CodeExecutor.run(nil, {code: [[0x22].pack('C')]}, ({0 => 99}), stack)
    assert_equal [99], stack
  end

  def test_fload_1_0x23
    stack = []
    JRE::CodeExecutor.run(nil, {code: [[0x23].pack('C')]}, ({1 => 99}), stack)
    assert_equal [99], stack
  end

  def test_fload_2_0x24
    stack = []
    JRE::CodeExecutor.run(nil, {code: [[0x24].pack('C')]}, ({2 => 99}), stack)
    assert_equal [99], stack
  end

  def test_fload_3_0x25
    stack = []
    JRE::CodeExecutor.run(nil, {code: [[0x25].pack('C')]}, ({3 => 99}), stack)
    assert_equal [99], stack
  end

  def test_dload_0_0x26
    stack = []
    JRE::CodeExecutor.run(nil, {code: [[0x26].pack('C')]}, ({0 => 99}), stack)
    assert_equal [99], stack
  end

  def test_dload_1_0x27
    stack = []
    JRE::CodeExecutor.run(nil, {code: [[0x27].pack('C')]}, ({1 => 99}), stack)
    assert_equal [99], stack
  end

  def test_dload_2_0x28
    stack = []
    JRE::CodeExecutor.run(nil, {code: [[0x28].pack('C')]}, ({2 => 99}), stack)
    assert_equal [99], stack
  end

  def test_dload_3_0x29
    stack = []
    JRE::CodeExecutor.run(nil, {code: [[0x29].pack('C')]}, ({3 => 99}), stack)
    assert_equal [99], stack
  end

  def test_aload_0_0x2a
    stack = []
    JRE::CodeExecutor.run(nil, {code: [[0x2a].pack('C')]}, ({0 => 99}), stack)
    assert_equal [99], stack
  end

  def test_aload_1_0x2b
    stack = []
    JRE::CodeExecutor.run(nil, {code: [[0x2b].pack('C')]}, ({1 => 99}), stack)
    assert_equal [99], stack
  end

  def test_aload_2_0x2c
    stack = []
    JRE::CodeExecutor.run(nil, {code: [[0x2c].pack('C')]}, ({2 => 99}), stack)
    assert_equal [99], stack
  end

  def test_aload_3_0x2d
    stack = []
    JRE::CodeExecutor.run(nil, {code: [[0x2d].pack('C')]}, ({3 => 99}), stack)
    assert_equal [99], stack
  end

  def test_istore_0x36
    stack = [99]
    lvars = {}
    JRE::CodeExecutor.run(nil, {code: [[0x36].pack('C'), [2].pack('C')]}, lvars, stack)
    assert_equal 99, lvars[2]
  end

  def test_lstore_0x37
    stack = [99]
    lvars = {}
    JRE::CodeExecutor.run(nil, {code: [[0x37].pack('C'), [2].pack('C')]}, lvars, stack)
    assert_equal 99, lvars[2]
  end

  def test_fstore_0x38
    stack = [99]
    lvars = {}
    JRE::CodeExecutor.run(nil, {code: [[0x38].pack('C'), [2].pack('C')]}, lvars, stack)
    assert_equal 99, lvars[2]
  end

  def test_dstore_0x39
    stack = [99]
    lvars = {}
    JRE::CodeExecutor.run(nil, {code: [[0x39].pack('C'), [2].pack('C')]}, lvars, stack)
    assert_equal 99, lvars[2]
  end

  def test_astore_0x3a
    stack = [99]
    lvars = {}
    JRE::CodeExecutor.run(nil, {code: [[0x3a].pack('C'), [2].pack('C')]}, lvars, stack)
    assert_equal 99, lvars[2]
  end

  def test_istore_0_0x3b
    stack = [99]
    lvars = {}
    JRE::CodeExecutor.run(nil, {code: [[0x3b].pack('C')]}, lvars, stack)
    assert_equal 99, lvars[0]
  end

  def test_istore_1_0x3c
    stack = [99]
    lvars = {}
    JRE::CodeExecutor.run(nil, {code: [[0x3c].pack('C')]}, lvars, stack)
    assert_equal 99, lvars[1]
  end

  def test_istore_2_0x3d
    stack = [99]
    lvars = {}
    JRE::CodeExecutor.run(nil, {code: [[0x3d].pack('C')]}, lvars, stack)
    assert_equal 99, lvars[2]
  end

  def test_istore_3_0x3e
    stack = [99]
    lvars = {}
    JRE::CodeExecutor.run(nil, {code: [[0x3e].pack('C')]}, lvars, stack)
    assert_equal 99, lvars[3]
  end

  def test_lstore_0_0x3f
    stack = [99]
    lvars = {}
    JRE::CodeExecutor.run(nil, {code: [[0x3f].pack('C')]}, lvars, stack)
    assert_equal 99, lvars[0]
  end

  def test_lstore_1_0x40
    stack = [99]
    lvars = {}
    JRE::CodeExecutor.run(nil, {code: [[0x40].pack('C')]}, lvars, stack)
    assert_equal 99, lvars[1]
  end

  def test_lstore_2_0x41
    stack = [99]
    lvars = {}
    JRE::CodeExecutor.run(nil, {code: [[0x41].pack('C')]}, lvars, stack)
    assert_equal 99, lvars[2]
  end

  def test_lstore_3_0x42
    stack = [99]
    lvars = {}
    JRE::CodeExecutor.run(nil, {code: [[0x42].pack('C')]}, lvars, stack)
    assert_equal 99, lvars[3]
  end

  def test_fstore_0_0x43
    stack = [99]
    lvars = {}
    JRE::CodeExecutor.run(nil, {code: [[0x43].pack('C')]}, lvars, stack)
    assert_equal 99, lvars[0]
  end

  def test_fstore_1_0x44
    stack = [99]
    lvars = {}
    JRE::CodeExecutor.run(nil, {code: [[0x44].pack('C')]}, lvars, stack)
    assert_equal 99, lvars[1]
  end

  def test_fstore_2_0x45
    stack = [99]
    lvars = {}
    JRE::CodeExecutor.run(nil, {code: [[0x45].pack('C')]}, lvars, stack)
    assert_equal 99, lvars[2]
  end

  def test_fstore_3_0x46
    stack = [99]
    lvars = {}
    JRE::CodeExecutor.run(nil, {code: [[0x46].pack('C')]}, lvars, stack)
    assert_equal 99, lvars[3]
  end

  def test_dstore_0_0x47
    stack = [99]
    lvars = {}
    JRE::CodeExecutor.run(nil, {code: [[0x47].pack('C')]}, lvars, stack)
    assert_equal 99, lvars[0]
  end

  def test_dstore_1_0x48
    stack = [99]
    lvars = {}
    JRE::CodeExecutor.run(nil, {code: [[0x48].pack('C')]}, lvars, stack)
    assert_equal 99, lvars[1]
  end

  def test_dstore_2_0x49
    stack = [99]
    lvars = {}
    JRE::CodeExecutor.run(nil, {code: [[0x49].pack('C')]}, lvars, stack)
    assert_equal 99, lvars[2]
  end

  def test_dstore_3_0x4a
    stack = [99]
    lvars = {}
    JRE::CodeExecutor.run(nil, {code: [[0x4a].pack('C')]}, lvars, stack)
    assert_equal 99, lvars[3]
  end

  def test_astore_0_0x4b
    stack = [99]
    lvars = {}
    JRE::CodeExecutor.run(nil, {code: [[0x4b].pack('C')]}, lvars, stack)
    assert_equal 99, lvars[0]
  end

  def test_astore_1_0x4c
    stack = [99]
    lvars = {}
    JRE::CodeExecutor.run(nil, {code: [[0x4c].pack('C')]}, lvars, stack)
    assert_equal 99, lvars[1]
  end

  def test_astore_2_0x4d
    stack = [99]
    lvars = {}
    JRE::CodeExecutor.run(nil, {code: [[0x4d].pack('C')]}, lvars, stack)
    assert_equal 99, lvars[2]
  end

  def test_astore_3_0x4e
    stack = [99]
    lvars = {}
    JRE::CodeExecutor.run(nil, {code: [[0x4e].pack('C')]}, lvars, stack)
    assert_equal 99, lvars[3]
  end

  def test_pop_0x57
    stack = [99]
    JRE::CodeExecutor.run(nil, {code: [[0x57].pack('C')]}, nil, stack)
    assert_equal [], stack
  end

  def test_dup_0x59
    stack = [99]
    JRE::CodeExecutor.run(nil, {code: [[0x59].pack('C')]}, nil, stack)
    assert_equal [99, 99], stack
  end

  def test_i2l_0x85
    stack = [99]
    JRE::CodeExecutor.run(nil, {code: [[0x85].pack('C')]}, nil, stack)
    assert_equal 1, stack.size
    assert_equal 99, stack[0].value
    assert_equal JRE::Type::Long, stack[0].class
  end

  def test_l2i_0x88
    stack = [JRE::Type::Long.new(99)]
    JRE::CodeExecutor.run(nil, {code: [[0x88].pack('C')]}, nil, stack)
    assert_equal 1, stack.size
    assert_equal 99, stack[0]
    assert_equal Fixnum, stack[0].class
  end

  def test_lcmp_0x94_greater
    stack = [JRE::Type::Long.new(100), JRE::Type::Long.new(99)]
    JRE::CodeExecutor.run(nil, {code: [[0x94].pack('C')]}, nil, stack)
    assert_equal 1, stack.size
    assert_equal 1, stack[0]
  end

  def test_lcmp_0x94_equal
    stack = [JRE::Type::Long.new(99), JRE::Type::Long.new(99)]
    JRE::CodeExecutor.run(nil, {code: [[0x94].pack('C')]}, nil, stack)
    assert_equal 1, stack.size
    assert_equal 0, stack[0]
  end

  def test_lcmp_0x94_less
    stack = [JRE::Type::Long.new(90), JRE::Type::Long.new(98)]
    JRE::CodeExecutor.run(nil, {code: [[0x94].pack('C')]}, nil, stack)
    assert_equal 1, stack.size
    assert_equal -1, stack[0]
  end

  def test_ireturn_0xac
    stack = [199]
    value = JRE::CodeExecutor.run(nil, {code: [[0xac].pack('C')]}, nil, stack)
    assert_equal 0, stack.size
    assert_equal 199, value
  end

  def test_lreturn_0xad
    stack = [JRE::Type::Long.new(99)]
    value = JRE::CodeExecutor.run(nil, {code: [[0xad].pack('C')]}, nil, stack)
    assert_equal 0, stack.size
    assert_equal JRE::Type::Long, value.class
    assert_equal 99, value.value
  end

  def test_areturn_0xb0
    o = Object.new
    stack = [o]
    value = JRE::CodeExecutor.run(nil, {code: [[0xac].pack('C')]}, nil, stack)
    assert_equal 0, stack.size
    assert_equal o, value
  end

  def test_return_0xb1
    stack = []
    value = JRE::CodeExecutor.run(nil, {code: [[0xb1].pack('C')]}, nil, stack)
    assert_equal 0, stack.size
    assert_equal :void, value
  end
end
