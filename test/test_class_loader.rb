require 'minitest/autorun'
require 'jre/class_loader/class_loader'
require 'jre/types/long'

class ClassLoaderTest < Minitest::Test
  def test_good_file
    jclass = JRE::ClassLoader.load_file('test/Test.class')
    assert jclass
    assert jclass.constant_pool.size > 0
    assert jclass.access_flags.include? :ACC_PUBLIC
    assert_equal 'Test', jclass.this_name
    assert_equal 'java/lang/Object', jclass.super_name
    assert_equal ["java/io/Serializable"], jclass.interfaces
    assert_equal 4, jclass.jfields.size
    assert jclass.jfields['ivar']
    assert_equal "Ljava/lang/String;", jclass.jfields['SCONST'][:type]
    assert_equal JRE::Type::Long, jclass.jfields['serialVersionUID'][:value].class
    assert_equal 42, jclass.jfields['serialVersionUID'][:value].value
    assert_equal 7, jclass.jmethods.size
    assert jclass.jmethods['getConst']['()I'][:code]
    assert jclass.jmethods['add']['(II)I'][:code]
  end

  def test_fail_on_non_class_file_load
    assert_raises(RuntimeError, 'This is not compiled java class') {
      JRE::ClassLoader.load_file('test/Test.java')
    }
  end

  def test_fail_on_non_existing_file
    assert_raises(Errno::ENOENT) {
      JRE::ClassLoader.load_file('test/No.such.file')
    }
  end
end
